## Program Overview 

Thank you for your interest in partnering with GitLab. Through the corporate philanthropy program, GitLab supports registered nonprofit organizations with monetary contributions. 

## Eligibility 

GitLab supports registered 501c3 (or country equivalent) nonprofit organizations in good standing that align with our [Values](https://about.gitlab.com/handbook/values/). A “Registered Nonprofit Organization” is one that has been registered with the local government or authorized agency within its applicable local, state, provincial, federal, or national government. 

GitLab prioritizes financial contributions to organizations that align with our values, support our environmental and social goals, and help us advance our mission to make it so that everyone can contribute. Our current social and environmental focus areas are:

- Diversity, Inclusion, and Belonging 
- Talent Management 
- Climate Action

 
## Application Process

There are two ways that team members or organizations can request support. Please check the box next to your request type and follow the steps outlined.

- [ ] I want to request funding from the ESG team to support a registered nonprofit organization OR
- [ ] I have department or TMRG  budget that I would like to utilize to support a registered nonprofit organization

## Requesting funding from the ESG Team

At this time we are not accepting applications. If you would like to submit an organization to be considered in the future, please answer the following questions to the best of your ability, and add the tag `ESG::Ready for Review`. You will be notified if there is a future partnership opportunity.

- [ ] What is the name of the nonprofit organization?
- [ ] Is the organization a registered 501c3 nonprofit or country equivalent in good standing? If so, please upload an I9, W9 or other documentation showing registered nonprofit status.
- [ ] Is the organization a GitLab customer?
- [ ] What is the mission of the nonprofit?
- [ ] How does this nonprofit align with GitLab’s values and mission?
- [ ] What is the estimated monetary amount you’d like to request from GitLab?
- [ ] Are you requesting support for a specific program or event? If so, please add the details or upload a partnership package.
- [ ] Please provide any other relevant details.

## Department or TMRG donation request

If you have department or TMRG  budget that you would like to utilize to support a registered nonprofit organization, please answer the following questions to the best of your ability and add the tag `ESG::Ready for Review`. Then, complete the steps under ‘review process.’

- [ ] What is the name of the organization?
- [ ] Is the organization a registered 501c3 nonprofit or country equivalent in good standing? If so, please upload an I9, W9 or other documentation showing registered nonprofit status. 
- [ ] Is the organization a GitLab customer? 
- [ ] What is the mission of the nonprofit?
- [ ] How does the nonprofit align with GitLab’s values and mission? 
- [ ] What is the estimated monetary contribution?
- [ ] Is this request for a specific program or event? If so, please add the details. 
- [ ] Please add the department name or TMRG funding this request. 
- [ ] Please provide any other relevant details. 

## Review process

The team member submitting the issue is responsible for obtaining proper approvals and working with Accounts Payable to issue the payment.

Approvers:

- [ ] ESG (@slcline) - verification that the organization meets GitLab’s philanthropy policy and requirements 
- [ ] FP&A (@cam.smith) - verification that the budget is available
- [ ] DIB (@sheridam, @lmcnally1) - verification from the DIB team if this is a TMRG request

Once approvals are completed, the team member requesting the donation needs to obtain an invoice from the non-profit that contains the bank payment details and submit this to AP@GitLab.com. Please check the following boxes once completed.

- [ ] Invoice submitted to AP@GitLab.com
- [ ] Confirmation that the donation has been made. Please close the issue after confirmation.

/assign @slcline, @Askeete
/label ~ESG::Ready for Review ~monetary-support
/epic https://gitlab.com/groups/gitlab-com/-/epics/2145
/confidential
