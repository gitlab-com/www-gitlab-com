---
features:
  secondary:
  - name: "Improved project creation permission settings"
    available_in: [core, premium, ultimate]
    gitlab_com: true
    add_ons: []
    documentation_link: 'https://docs.gitlab.com/administration/settings/visibility_and_access_controls/#define-which-roles-can-create-projects'
    image_url: '/images/unreleased/updated_project_creation_protection.png'
    reporter: lohrc
    stage: tenant_scale
    categories:
    - Groups & Projects
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/507410'
    description: |
      We've improved the project creation permission settings to make them more clear, intuitive, and aligned with our security principles. The improved settings include:
      - Renamed the "Default project creation protection" dropdown to "Minimum role required for project creation" to clearly reflect the setting's purpose.
      - Renamed the "Developers + Maintainers" dropdown option to "Developers" for consistency across the platform.
      - Reordered the dropdown options from most restrictive to least restrictive access level.

      These changes make it easier to understand and configure which roles can create projects within your groups, helping administrators enforce appropriate access controls more confidently.

      Thank you [@yasuk](https://gitlab.com/yasuk) for this community contribution!
