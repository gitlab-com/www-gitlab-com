features:
  primary:
  - name: "Enhanced merge request reviewer assignments"
    available_in: [premium, ultimate]  # Include all supported tiers
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/reviews/#request-a-review'
    image_url: '/images/17_6/create-enhanced-reviewer-assignment.png'
    reporter: phikai
    stage: create
    categories:
      - 'Code Review Workflow'
    epic_url:
      - 'https://gitlab.com/groups/gitlab-org/-/epics/12878'
    description: |
      After you've carefully crafted your changes and prepared a merge request, the next step is to identify reviewers who can help move it forward. Identifying the right reviewers for your merge request involves understanding who the right approvers are, and who might be a subject matter expert (CODEOWNER) for the changes you're proposing. 

      Now, when assigning reviewers, the sidebar creates a connection between the approval requirements for your merge request and reviewers. View each approval rule, then select from approvers who can satisfy that approval rule and move the merge request forward for you. If you use [optional CODEOWNER sections](https://docs.gitlab.com/ee/user/project/codeowners/#make-a-code-owners-section-optional) those rules are also shown in the sidebar to help you identify appropriate subject matter experts for your changes.

      Enhanced reviewer assignments is the next evolution of applying intelligence to assigned reviewers in GitLab. This iteration builds on what we've learned from suggested reviewers, and how to effectively identify the best reviewers for moving a merge request forward. In [upcoming iterations](https://gitlab.com/groups/gitlab-org/-/epics/14808) of reviewer assignments, we'll continue to enhance the intelligence used to recommend and rank possible reviewers.
