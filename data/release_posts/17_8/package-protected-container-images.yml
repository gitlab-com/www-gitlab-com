---
features:
  top:
  - name: "Enhance security with protected container repositories"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/user/packages/container_registry/container_repository_protection_rules.html'
    image_url: '/images/17_8/protected_containers.png'
    reporter: trizzi
    stage: package
    categories:
    - 'Container Registry'
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/480385'
    description: |
      We're thrilled to announce the rollout of protected container repositories, a new feature in GitLab's container registry that addresses security and control challenges in managing container images. Organizations often struggle with unauthorized access to sensitive container repositories, accidental modifications, lack of granular control, and difficulties in maintaining compliance. This solution provides enhanced security through strict access controls, granular permissions for push, pull, and management operations, and seamless integration with GitLab CI/CD pipelines.

      Protected container repositories offers value to users by reducing the risk of security breaches and accidental changes to critical assets. This feature streamlines workflows by maintaining security without sacrificing development speed, improves overall governance of the container registry, and provides peace of mind knowing that important container assets are protected according to organizational needs. 

      This feature and the [protected packages](https://gitlab.com/groups/gitlab-org/-/epics/5574) feature are both community contributions from `gerardo-navarro` and the Siemens crew. Thank you Gerardo and the rest of the crew from Siemens for their many contributions to GitLab! If you are interesting in learning more about how Gerardo and the Siemens crew contributed this change, check out this [video](https://www.youtube.com/watch?v=5-nQ1_Mi7zg) in which Gerardo shares his learnings and best practices for contributing to GitLab based on his experience as an external contributor.

