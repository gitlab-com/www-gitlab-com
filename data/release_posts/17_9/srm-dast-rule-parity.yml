features:
  primary:
  - name: "Configure DAST scans through the UI with full control"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/dast/on-demand_scan.html'
    image_url: '/images/17_9/dast_parity.png'
    reporter: smeadzinger
    stage: application_security_testing
    categories:
      - 'DAST'
    issue_url:
      - 'https://gitlab.com/groups/gitlab-org/-/epics/16057'
    description: |
      To effectively test complex applications, security teams need flexibility when they configure DAST scans. Previously, DAST scans configured through the UI had limited configuration options, which prevented successful scanning of applications with specific security requirements. This meant you had to use pipeline-based scans even for quick security assessments.

      You can now configure DAST scans through the UI with the same granular control available in pipeline-based scans. This includes:
      
      - Full authentication configuration, including custom headers and cookies
      - Precise crawl settings like maximum pages, maximum depth, and excluded URLs
      - Advanced scan timeouts and retry attempts
      - Custom scanner behavior, like maximum links to crawl and DOM depth
      - Targeted scanning modes for specific vulnerability types
      
      Save these configurations as reusable profiles to maintain consistent security testing across your applications. Every configuration change is tracked with audit events, so you know when scan settings are added, edited, or removed.
      
      This enhanced control helps you run more effective security scans while maintaining compliance using detailed audit trails. Instead of spending time managing pipeline configurations, you can quickly launch the right scan for each application to find and fix vulnerabilities faster.
