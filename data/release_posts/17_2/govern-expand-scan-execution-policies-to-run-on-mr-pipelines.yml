---
features:
  secondary:
  - name: "Expand \"Scan Execution Policies\" to run `latest` templates for each GitLab analyzer"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/policies/scan-execution-policies.html'
    reporter: g.hickman
    stage: software_supply_chain_security
    categories:
    - Security Policy Management
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/415427'
    image_url: '/images/17_2/latest-template-image.png'
    description: |
      [Scan execution policies](https://docs.gitlab.com/ee/user/application_security/policies/scan-execution-policies.html) have been expanded to allow you to choose between `default` and `latest` GitLab templates when defining the policy rules. While `default` reflects the current behavior, you may update your policy to `latest` to use features available only in the latest template of the given security analyzer. 

      By utilizing the `latest` template, you may now ensure scans are enforced on merge request pipelines, along with any other rules enabled in the `latest` template. Previously this was limited to branch pipelines or a specified schedule.
      
      Note: Be sure to review all changes between `default` and `latest` templates before modifying the policy to ensure this suits your needs!
