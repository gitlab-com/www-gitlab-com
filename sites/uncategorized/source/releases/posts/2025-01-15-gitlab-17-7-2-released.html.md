---
title: "GitLab Patch Release: 17.7.2"
categories: releases
author: Mayra Cabrera
author_gitlab: mayra-cabrera
author_twitter: gitlab
description: "GitLab releases 17.7.2"
tags: patch releases, releases
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.7.2 for GitLab Community Edition and Enterprise Edition.

These versions resolve a number of regressions and bugs. This patch release does not include any security fixes.

## GitLab Community Edition and Enterprise Edition

### 17.7.2

* [Merge branch 'azcopy-url-20250108' into '17-7-stable'](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2159)
* [Fixes issue](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/177296) where some merge request diffs with associated comments were not visible. This does not correct the display issue for existing records, but does prevent new instances of this occurrence.
* [Remove `download_code` dependency from access to read merge requests](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176667)
* [Fix handling of short gzip metadata files](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/177633)

## Important notes on upgrading

If you had previously upgraded to GitLab 17.7.0 or 17.7.1 this patch is recommended to prevent any further occurrences of merge request comments being unable to be displayed. A future release will correct the display issue for affected records.

This version does not include any new migrations, and for multi-node deployments, [should not require any downtime](https://docs.gitlab.com/ee/update/#upgrading-without-downtime).

Please be aware that by default the Omnibus packages will stop, run migrations,
and start again, no matter how “big” or “small” the upgrade is. This behavior
can be changed by adding a [`/etc/gitlab/skip-auto-reconfigure`](https://docs.gitlab.com/ee/update/zero_downtime.html) file,
which is only used for [updates](https://docs.gitlab.com/omnibus/update/README.html).

## Updating

To update, check out our [update page](/update/).

## GitLab subscriptions

Access to GitLab Premium and Ultimate features is granted by a paid [subscription](/pricing/).

Alternatively, [sign up for GitLab.com](https://gitlab.com/users/sign_in)
to use GitLab's own infrastructure.

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
