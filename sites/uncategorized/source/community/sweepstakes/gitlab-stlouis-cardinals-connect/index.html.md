---
layout: markdown_page
title: "2024 GitLab - St. Louis Cardinals Connect Event Sweepstakes - Official Rules"
description: "2024 GitLab - St. Louis Cardinals Connect Event Sweepstakes - Official Rules"
---
 
NO PURCHASE NECESSARY TO ENTER OR WIN. MAKING A PURCHASE OR PAYMENT OF ANY KIND WILL NOT INCREASE YOUR CHANCES OF WINNING. VOID WHERE PROHIBITED OR RESTRICTED BY LAW.

1. **PROMOTION DESCRIPTION**: The 2024 GitLab - St. Louis Cardinals Connect Event Sweepstakes ("Promotional Game") begins on 11 September 2024 at 7:00 PM CDT and ends on 11 September 2024 at 7:30 PM CDT (the "Promotion Period").

The sponsor of this Promotional Game is GitLab, Inc. ("Sponsor"). By participating in the Promotional Game, each Entrant unconditionally accepts and agrees to comply with and abide by these Official Rules and the decisions of Sponsor, which shall be final and binding in all respects. Sponsor is responsible for the collection, submission or processing of Entries and the overall administration of the Promotional Game. Entrants should look solely to Sponsor with any questions, comments or problems related to the Promotional Game. Sponsor may be reached by email at jwyatt@gitlab.com during the Promotion Period.

This Promotional Game is in no way sponsored, endorsed, or administered by the St. Louis Cardinals or Busch Stadium.

2. **ELIGIBILITY**: Open to legal residents of the United States who are at the age of majority in their locale (the "Entrant"). Nationals of the following countries are not eligible to participate in this Promotional Game: Syria, North Korea, Iran, Cuba, and the Province of Quebec in Canada. Sponsor, and their respective parents, subsidiaries, affiliates, distributors, retailers, sales representatives, advertising and promotion agencies and each of their respective officers, directors and employees (the "Promotion Entities"), are ineligible to enter the Promotional Game or win a prize. Household Members and Immediate Family Members of such individuals are also not eligible to enter or win. "Household Members" shall mean those people who share the same residence at least three months a year. "Immediate Family Members" shall mean parents, step-parents, legal guardians, children, step-children, siblings, step-siblings, or spouses. This Promotional Game is subject to all applicable federal, state and local laws and regulations and is void where prohibited or restricted by law.

**Because Sponsor is a contractor to the Government and because the sweepstakes may involve a random drawing for prizes, some of which may exceed USD $20.00 in value, Sponsor will require any government employee winner to provide written confirmation from their relevant agency ethics official that the prize is permissible before acceptance of any prize. Failure to provide the required written confirmation will result in forfeiture of the prize.**
 
 3. **PRIZES**:

  **First Prize**: One (1) winner will receive one (1) jersey, unisex (approximate retail value or "ARV": USD $250.00).

  **Second Prize**: One (1) winner will receive one (1) baseball, signed by a current St. Louis Cardinals team member; ARV: USD $150.00.

Total value of all prizes: ARV USD $400.00.

Only one prize per person and per household will be awarded. Gift cards and gift certificates are subject to the terms and conditions of the issuer. Prizes cannot be transferred, redeemed for cash or substituted by winner. **PRIZES WILL BE AWARDED AT THE GITLAB SUITE DURING THE PROMOTION PERIOD. WINNER MUST PICK UP THEIR PRIZE AT THE GITLAB SUITE ON 11 SEPTEMBER 2024 BY 8:00 PM CDT. ANY PRIZE NOT PICKED UP AT THE GITLAB SUITE WILL BE FORFEITED AND WILL NOT BE MAILED TO THE WINNER**. Sponsor reserves the right in its sole and absolute discretion to award a substitute prize of equal or greater value if a prize described in these Official Rules is unavailable or cannot be awarded, in whole or in part, for any reason. The ARV of the prize represents Sponsor's good faith determination. That determination is final and binding and cannot be appealed. If the actual value of the prize turns out to be less than the stated ARV, the difference will not be awarded in cash. Sponsor makes no representation or warranty concerning the appearance, safety or performance of any prize awarded. Restrictions, conditions, and limitations may apply. Sponsor will not replace any lost or stolen prize items.

This Promotional Game is open to legal residents of the United States and Prize will only be awarded and/or delivered to addresses within said locations. All federal, state and/or local taxes, fees, and surcharges are the sole responsibility of the prize winner. Failure to comply with the Official Rules will result in forfeiture of the prize.

4. **HOW TO ENTER**: Enter the Promotional Game during the Promotion Period by registering for and attending the 2024 GitLab Connect Event scheduled to be held on 11 September 2024, at Busch Stadium, in St. Louis, Missouri, USA. Entrants must attend the GitLab presentation and event, and be in attendance at the GitLab suite during the Promotion Period. Entrant will be required to provide their first name and last initial, on a piece of paper (“Entry/Entries”) which will be provided by Sponsor.

Automated or robotic Entries submitted by individuals or organizations will be disqualified. Internet entry must be made by the Entrant. Any attempt by Entrant to obtain more than the stated number of Entries by using multiple/different email addresses, identities, registrations, logins or any other methods, including, but not limited to, commercial contest/Promotional Game subscription notification and/or entering services, will void Entrant's Entries and that Entrant may be disqualified. Final eligibility for the award of any prize is subject to eligibility verification as set forth below. All Entries must be posted by the end of the Promotion Period in order to participate. 

Sponsor's database clock will be the official timekeeper for this Promotional Game.

5. **WINNER SELECTION**: The Winner(s) of the Promotional Game will be selected in a random drawing from among all eligible Entries received throughout the Promotion Period. The random drawing will be conducted on 11 September 2024 between approximately 7:30 PM and 7:45 PM CDT by Sponsor or its designated representatives, whose decisions are final. Odds of winning will vary depending on the number of eligible Entries received.

6. **WINNER NOTIFICATION**: Winner will be notified in person at the GitLab suite on 11 September 2024 between approximately 7:30 PM and 7:45 PM CDT. **WINNER MUST BE PRESENT AT THE GITLAB SUITE AT THE TIME OF THE WINNER NOTIFICATION, AND WILL NOT BE CONTACTED BY PHONE OR EMAIL. WINNER MUST PICK UP THEIR PRIZE AT THE GITLAB SUITE ON 11 SEPTEMBER 2024 BY 8:00 PM CDT**. Sponsor is not responsible for any delay or failure to receive notification for any reason, including inactive email account(s), technical difficulties associated therewith, or Winner’s failure to adequately monitor any email account.

Any winner notification not responded to or returned as undeliverable may result in prize forfeiture. The potential prize winner may be required to sign and return an affidavit of eligibility and release of liability, and a Publicity Release (collectively "the Prize Claim Documents"). No substitution or transfer of a prize is permitted except by Sponsor.

7. **PRIVACY**: Any personal information supplied by you will be subject to the privacy policy of the Sponsor posted at about.gitlab.com/privacy. By entering the Promotional Game, you grant Sponsor permission to share your email address and any other personally identifiable information with the other Promotional Game Entities for the purpose of administration and prize fulfillment, including use in a publicly available Winners list.

8. **LIMITATION OF LIABILITY**: Sponsor assumes no responsibility or liability for (a) any incorrect or inaccurate entry information, or for any faulty or failed electronic data transmissions; (b) any unauthorized access to, or theft, destruction or alteration of entries at any point in the operation of this Promotional Game; (c) any technical malfunction, failure, error, omission, interruption, deletion, defect, delay in operation or communications line failure, regardless of cause, with regard to any equipment, systems, networks, lines, satellites, servers, camera, computers or providers utilized in any aspect of the operation of the Promotional Game; (d) inaccessibility or unavailability of any network or wireless service, the Internet or website or any combination thereof; (e) suspended or discontinued Internet, wireless or landline phone service; or (f) any injury or damage to participant's or to any other person’s computer or mobile device which may be related to or resulting from any attempt to participate in the Promotional Game or download of any materials in the Promotional Game.

If, for any reason, the Promotional Game is not capable of running as planned for reasons which may include without limitation, infection by computer virus, tampering, unauthorized intervention, fraud, technical failures, or any other causes which may corrupt or affect the administration, security, fairness, integrity or proper conduct of this Promotional Game, the Sponsor reserves the right at its sole discretion to cancel, terminate, modify or suspend the Promotional Game in whole or in part. In such event, Sponsor shall immediately suspend all drawings and prize awards, and Sponsor reserves the right to award any remaining prizes (up to the total ARV as set forth in these Official Rules) in a manner deemed fair and equitable by Sponsor. Sponsor and Released Parties shall not have any further liability to any participant in connection with the Promotional Game.

9. **WINNER LIST/OFFICIAL RULES**: To view these Official Rules visit https://about.gitlab.com/community/sweepstakes/. For a winner list, contact jwyatt@gitlab.com.

10. **SPONSOR**: GitLab, Inc., 268 Bush Street, #350, San Francisco, CA 94104, jwyatt@gitlab.com.
   